export type IconDialogTypes = "yes" | "no" | "info" | "warning";

export interface DialogInterface {
  icon?: IconDialogTypes;
  title?: string;
  description?: string;
  isOpen?: boolean;
  isConfirmed?: boolean;
  isRejected?: boolean;
  isNext?: boolean;
  confirm?: boolean;
  reject?: boolean;
  next?: boolean;
}
