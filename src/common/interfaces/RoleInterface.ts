import { RoleTypes, RoleIdTypes, AbilityTypes, RoleColorTypes } from "./types";

export interface RoleInterface {
  id: RoleIdTypes;
  type: RoleTypes;
  name: string;
  shortName: string;
  priority: number;
  isActiveNight: boolean;
  isDisabled: boolean;
  desctiption: string;
  abilities: AbilityTypes[];
  color: RoleColorTypes;
}
