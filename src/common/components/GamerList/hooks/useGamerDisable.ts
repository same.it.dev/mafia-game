export const useGamerDisable =
  () => (id: number, disableGamerId?: number | number[]) => {
    const isArray = typeof disableGamerId === "object";

    if (isArray) return disableGamerId.includes(id);

    return disableGamerId === id;
  };
