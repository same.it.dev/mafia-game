import { RoleColorTypes, RoleIdTypes, RoleTypes } from "common/interfaces";
import { ReactNode } from "react";

export type GamerListElementVariantType = "submit" | "cancel";

export interface GamerListInterface {
  id: number;
  roleId: RoleIdTypes;
  isActive: boolean;
  isKilled: boolean;
  isBlocked: boolean;
  name: string;
  shortName: string;
  type: RoleTypes;
  selected: boolean;
  color: RoleColorTypes;
}

export interface GamerListElementProps {
  variant: GamerListElementVariantType;
  onSubmit?: (data: {
    gamer: GamerListInterface;
    gamers: GamerListInterface[];
  }) => void;
  children: ReactNode;
}

export interface GamerListActionsInterface {
  reset: () => void;
}

export type GamerListSubmitType = (data: {
  gamers: GamerListInterface[];
  gamer: GamerListInterface | null;
  actions?: GamerListActionsInterface;
}) => void;

export type onGamerListSelectGamersInterface = (
  gamers: GamerListInterface[],
  actions: GamerListActionsInterface
) => void;

export type onGamerListSelectGamerInterface = (
  gamer: GamerListInterface,
  actions: GamerListActionsInterface
) => void;
