/* eslint-disable no-restricted-globals */
import { useDispatch, setDialog, selectDialog, useSelector } from "redux-store";

export const useMainDialog = () => {
  const dispatch = useDispatch();
  const { title, description, isOpen, icon, confirm, next, reject } =
    useSelector(selectDialog);

  const onConfirm = () => {
    dispatch(setDialog({ isOpen: false, isConfirmed: true }));
  };

  const onNext = () => {
    dispatch(setDialog({ isOpen: false, isNext: true }));
  };

  const onReject = () => {
    dispatch(setDialog({ isOpen: false, isRejected: true }));
  };

  return {
    onConfirm,
    onNext,
    onReject,
    title,
    description,
    isOpen,
    icon,
    confirm,
    next,
    reject,
  };
};
