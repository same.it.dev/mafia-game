import { Dialog } from "common/components";
import { useMainDialog } from "./hooks";

export const MainDialog = () => {
  const {
    icon,
    title,
    description,
    isOpen,
    onConfirm,
    onNext,
    onReject,
    confirm,
    next,
    reject,
  } = useMainDialog();

  return (
    <Dialog
      icon={icon}
      title={title}
      description={description}
      isOpen={isOpen}
      confirm={confirm}
      reject={reject}
      next={next}
      onConfirm={onConfirm}
      onNext={onNext}
      onReject={onReject}
    />
  );
};
