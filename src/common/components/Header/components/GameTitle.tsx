import { Typography, Box } from "@mui/material";

export const GameTitle = () => {
  return (
    <Box
      sx={{
        display: "flex",
        width: "100%",
        justifyContent: "center", // Центрирование содержимого
        margin: "20px 0", // Отступы сверху и снизу
        backgroundColor: "transparent", // Прозрачный фон (можете изменить по желанию)
      }}
    >
      <Typography
        variant="h1"
        sx={{
          color: "primary.main", // Цвет текста из темы
          fontSize: { xs: "2rem", sm: "2.5rem", md: "3rem" }, // Адаптивный размер шрифта
          fontWeight: "bold", // Жирное начертание
          textShadow: "2px 2px 4px rgba(0,0,0,0.5)", // Тень текста для лучшей читаемости
          textAlign: "center", // Центрирование текста
        }}
      >
        Mafia Game Org
      </Typography>
    </Box>
  );
};
