import { GamerList } from "common/components";
import { useAbility } from "common/hooks";
import { GamerPropsInterface } from "../../interfaces";

import { useMafia } from "./hooks";
import { useMafiaGamersIds } from "features/gamers/hooks";

export const Mafia = ({ gamer, onFinishAbility }: GamerPropsInterface) => {
  const ability = useAbility(gamer.role.abilities[0]);

  const { onChangeGamerId } = useMafia(onFinishAbility, gamer.id);
  const disableGamerId = useMafiaGamersIds();

  return (
    <>
      <GamerList
        disableGamerId={disableGamerId}
        title={`Використати здібність "${ability.name}"`}
        onGamerSelect={onChangeGamerId}
      />
    </>
  );
};
