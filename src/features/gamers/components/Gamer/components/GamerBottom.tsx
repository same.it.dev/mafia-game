/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

export const GamerBottom = ({ children }: Props) => (
  <div
    css={css`
      bottom: 0;
      width: 100%;
      display: flex;
      flex-direction: column;
      margin-top: 15px;
    `}
  >
    {children}
  </div>
);
