import { useSelector, selectGamers } from "redux-store";

export const useMafiaGamersIds = () => {
  const dataGamers = useSelector(selectGamers);

  return dataGamers
    .filter(({ role: { type } }) => type === "mafia")
    .map(({ id }) => id);
};
