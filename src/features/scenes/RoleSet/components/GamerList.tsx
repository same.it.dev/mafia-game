import { Button, Grid } from "@mui/material";

interface Props {
  gamersIds: Number[];
}

export const GamerList = ({ gamersIds }: Props) => (
  <Grid container spacing={2} style={{ marginTop: 15 }}>
    {gamersIds.map((id) => (
      <Grid key={`${id}`} item xs={3}>
        <Button
          style={{
            width: "65px",
            margin: 0,
          }}
          variant="contained"
        >{`${id}`}</Button>
      </Grid>
    ))}
  </Grid>
);
