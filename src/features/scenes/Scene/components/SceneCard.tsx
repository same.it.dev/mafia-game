import { Box } from "@mui/system";
import { ReactNode } from "react";

interface Props {
  children: ReactNode;
}

export const SceneCard = ({ children }: Props) => <Box>{children}</Box>;
