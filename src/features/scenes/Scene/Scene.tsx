/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useStartGame } from "common/hooks/useStartGame";
import { SceneCard } from "./components";
import { useScene } from "./hooks";

export const Scene = () => {
  const SceneComponent = useScene();
  const { isStartGame } = useStartGame();

  if (!isStartGame) return <></>;

  return (
    <div
      css={css`
        padding-bottom: 30px;
        position: relative;
      `}
    >
      <SceneCard>
        <SceneComponent />
      </SceneCard>
    </div>
  );
};
