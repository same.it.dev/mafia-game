import { Box, Button, Typography } from "@mui/material";
import { RolesList, SettingForm } from "./components";
import { useSetting } from "./hooks";

export const Setting = () => {
  const {
    onSubmit,
    onCountGamersChange,
    onPresetChange,
    gamerItems,
    presets,
    roles,
    countGamers,
  } = useSetting();

  return (
    <Box
      sx={{
        padding: "0 15px",
      }}
    >
      <Typography variant="h1">Налаштування</Typography>

      <SettingForm
        onSubmit={onSubmit}
        onCountGamersChange={onCountGamersChange}
        onPresetChange={onPresetChange}
        gamerItems={gamerItems}
        presets={presets}
      >
        <Button variant="contained" type="submit">
          Зберегти
        </Button>
        {countGamers ? <RolesList roles={roles} /> : ""}
      </SettingForm>
    </Box>
  );
};
